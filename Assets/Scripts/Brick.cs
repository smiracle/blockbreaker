﻿using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour {

	public AudioClip audioClip;
	public static int breakableCount = 0;
	public Sprite[] hitSprites;
	private int timesHit;
	private LevelManager levelManager;
	private bool isBreakable;
	public GameObject smoke;
	
	// Use this for initialization
	void Start () {		
		isBreakable = (this.tag=="Breakable");
		
		if(isBreakable)
		{
			breakableCount++;
		}
		
		levelManager=GameObject.FindObjectOfType<LevelManager>();
		timesHit = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnCollisionEnter2D(Collision2D col)
	{		
		//AudioSource.PlayClipAtPoint(audioClip,transform.position);
		if(isBreakable)
		{
			HandleHits();
		}
	}
	
	
	
	void HandleHits ()
	{
		timesHit++;	
		int maxHits = hitSprites.Length + 1;
		if(timesHit>=maxHits)
		{
			breakableCount--;
			print (breakableCount);
			levelManager.BrickDestroyed();
			
			GameObject smokePuff = (GameObject)Instantiate (smoke, gameObject.transform.position, Quaternion.identity);
			smokePuff.GetComponent<ParticleSystem>().startColor = gameObject.GetComponent<SpriteRenderer>().color;
			Destroy(gameObject);
		}
		else
		{
			LoadSprites();
		}
		//SimulateWin();	
	}
	
	void LoadSprites()
	{
		int spriteIndex = timesHit-1;
		if(hitSprites[spriteIndex])
		{
			this.GetComponent<SpriteRenderer>().sprite = hitSprites[spriteIndex];
		}
	}
	
	//TODO //Remove this method once we can win
	void SimulateWin()
	{
		print ("win");
		levelManager.LoadNextLevel();
	}
}
