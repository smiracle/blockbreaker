﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour {

	private float mousePosInBlocks;
	public static bool autoPlay = false;
	private Ball ball;
		
	void Start()
	{
		ball = GameObject.FindObjectOfType<Ball>();
	}
	
	// Update is called once per frame
	void Update () {
		
		if(!autoPlay)
		{
			MoveWithMouse();
		}
		else
		{
			AutoPlay();
		}
	}
	
	void AutoPlay()
	{
		Vector3 paddlePos = new Vector3(0.5f, this.transform.position.y, 0f);
		Vector3 ballPos = ball.transform.position;
		paddlePos.x = Mathf.Clamp (ballPos.x, 0.5f, 15.5f);
		this.transform.position = paddlePos;		
	}
	
	void MoveWithMouse()
	{
		mousePosInBlocks = Input.mousePosition.x / Screen.width * 16;					
		Vector3 paddlePos = new Vector3(Mathf.Clamp(mousePosInBlocks, .5f, 15.5f), this.transform.position.y, 0f);
		this.transform.position = paddlePos;
	}
}
