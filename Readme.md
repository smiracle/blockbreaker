### Block Breaker

My Breakout clone, created in 2017. Click to release the ball and bounce it to break the blocks. When running on Itch it's best to click the fullscreen icon, otherwise the edges are hidden.

-Three levels with increasing difficulty and 4 block types
-Backgrounds, music and sound effects